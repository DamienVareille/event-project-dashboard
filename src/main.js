import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import { store } from './shared/store'
import { i18n } from './plugins/i18n';
import VueCookie from 'vue-cookie'
import babelPolyfill from 'babel-polyfill'
import VueJWT from 'vuejs-jwt'

Vue.config.performance = true;
Vue.use(CoreuiVue);
Vue.use(VueCookie);
Vue.use(require('vue-moment'));
Vue.use(VueJWT);
Vue.prototype.$log = console.log.bind(console);


new Vue({
  el: '#app',
  router,
  store,
  icons,
  i18n,
  babelPolyfill,
  template: '<App/>',
  components: {
    App
  }
});
