import { httpClient } from './http-client'

function post(data) {
    return httpClient.post('api/users', data);
}

function activateUserAccount(userId) {
    const data = { activated: true };
    return httpClient.put(`api/users/${userId}`, data);
}

function getRelatedProvider(userId) {
    return httpClient.get(`api/users/${userId}/provider`);
}

export const userService = {
    post,
    activateUserAccount,
    getRelatedProvider
};
