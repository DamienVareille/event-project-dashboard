import { httpClient } from './http-client'

function login(email, password) {
    return httpClient.post('authentication_token',{
        email,
        password
    })
}

export const APIConnectionService = {
    login
};
