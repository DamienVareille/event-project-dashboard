import axios from 'axios';
import router from '../../router/index'
import {cookies} from '../services/cookies'

/** Default config for axios instance */
let config = {
    baseURL: process.env.VUE_APP_API_URL
};

/** Creating the instance for axios */
const httpClient = axios.create(config);

/** Auth token interceptors */
const authInterceptor = config => {
    if (cookies.getJWTToken() !== undefined && cookies.getJWTToken() !== null){
        config.headers.authorization = `Bearer ${cookies.getJWTToken()}`;
    }
    return config;
};

/** logger interceptors */
const loggerInterceptor = config => {
    /** TODO */
    return config;
};

/** Adding the request interceptors */
httpClient.interceptors.request.use(authInterceptor);
httpClient.interceptors.request.use(loggerInterceptor);

/** Adding the response interceptors */
httpClient.interceptors.response.use(
    response => {
        response = response.data;
        return response;
    },
    error => {
        if (error.response.status === 401 && error.response.config.url !== "authentication_token") {
            cookies.logout();
            router.push('/login');
        }
        return Promise.reject(error);
    }
);

export { httpClient };
