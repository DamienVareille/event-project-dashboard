import { httpClient } from './http-client'

function getStateByConstantCode(constantCode) {
    return httpClient.get(`api/request_states?constantCode=${constantCode}`).then((res) => {
        return res['hydra:member']
    });
}

export const requestState = {
    getStateByConstantCode,
};
