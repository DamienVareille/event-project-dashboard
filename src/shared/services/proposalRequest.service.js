import { httpClient } from './http-client'

function getAllPending() {
    return httpClient.get("api/proposal_requests?requestState.constantCode=PENDING").then((res) => {
        return res['hydra:member']
    });
}

function ignoreProposalRequest(requestId, apiStateId) {
    const data = { requestState: apiStateId };
    return httpClient.put(requestId, data);
}

export const proposalRequestService = {
    getAllPending,
    ignoreProposalRequest
};
