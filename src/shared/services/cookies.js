import Vue from 'vue';

const EXPIRES = 1;

let state = {
    connected: false
};

const login = (token, refreshToken) => {
    const payload = Vue.$jwt.decode(token);
    Vue.cookie.set('userJWT', token, EXPIRES);
    Vue.cookie.set('userRefreshToken', refreshToken, EXPIRES);
    Vue.cookie.set('userId', payload['user_id'], EXPIRES);
    state.connected = true;
};

const logout = () => {
    Vue.cookie.delete('userJWT');
    Vue.cookie.delete('userRefreshToken');
    Vue.cookie.delete('JWTPayload');
    state.connected = false;
};

const getJWTToken = () => {
  return Vue.cookie.get('userJWT');
};

const getRefreshToken = () => {
  return Vue.cookie.get('userRefreshToken');
};

const getUserId = () => {
  return Vue.cookie.get('userId');
};

export const cookies = {
    login,
    logout,
    getJWTToken,
    getRefreshToken,
    getUserId
};
