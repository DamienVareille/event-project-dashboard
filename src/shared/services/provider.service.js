import { httpClient } from './http-client'

function post(data) {
    return httpClient.post('api/providers', data);
}

export const providerService = {
    post
};
