import {userService} from "../services";

export const provider = {
    namespaced: true,
    strict: true,
    state: {
        currentProvider: null
    },
    actions: {
        addCurrentProviderByUserId({commit}, userId) {
            userService.getRelatedProvider(userId).then((res) => {
                commit('ADD_CURRENT_PROVIDER', res);
            })
        }
    },
    mutations: {
        ADD_CURRENT_PROVIDER(state, provider) {
            state.currentProvider = provider;
        }
    },
    getters: {
        currentProviderIsComplete: (state) => {
            const provider = state.currentProvider;
            if(provider === null) {
                return true;
            }
            return provider.city && provider.description && provider.phoneNumber;
        }
    }

};
