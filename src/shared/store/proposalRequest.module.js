import {proposalRequestService} from "../services";
import { requestState } from "../services";

export const proposalRequest = {
    namespaced: true,
    strict: true,
    state: {
        requests: [],
        fetchingRequests: false,
        ignoringRequest: false
    },
    actions: {
        async getAllPending({commit}) {
            commit('FETCHING_REQUEST', true);
            const pendingState = await requestState.getStateByConstantCode('PENDING').then(res => {
                return res[0]['@id'];
            });
            await proposalRequestService.getAllPending(pendingState).then((res) => {
                commit('FETCHING_REQUEST', false);
                commit('ADD_REQUESTS', res);
            })
        },
        async ignoreProposalRequest({commit}, requestId) {
            commit('IGNORING_REQUEST', true);
            const refutedState = await requestState.getStateByConstantCode('REFUTED').then(res => {
                return res[0]['@id'];
            });
            await proposalRequestService.ignoreProposalRequest(requestId, refutedState).then(() => {
                commit('IGNORING_REQUEST', false);
            });
        }
    },
    mutations: {
        ADD_REQUESTS(state, requests) {
            state.requests = requests;
        },
        FETCHING_REQUEST(state, isFetching) {
            state.fetchingRequests = isFetching;
        },
        IGNORING_REQUEST(state, isIgnoring) {
            state.ignoringRequest = isIgnoring;
        }
    },
    getters: {
        countRequests: (state) => {
            return state.requests.length
        },
        fetchingRequests: (state) => {
            return state.fetchingRequests
        },
        requests: (state) => {
            return state.requests
        },
        ignoringRequest: (state) => {
            return state.ignoringRequest
        }
    }

};
