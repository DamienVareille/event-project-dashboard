import Vue from 'vue';
import Vuex from 'vuex';
import {sidebar} from "./sidebar.module";
import {proposalRequest} from './proposalRequest.module'
import {provider} from "./provider.module";

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        sidebar,
        proposalRequest,
        provider
    },
});
